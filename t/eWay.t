# Before `make install' is performed this script should be runnable with
# `make test'. After `make install' it should work as `perl eWay.t'

use Test;
BEGIN { plan tests => 24 };
use Business::OnlinePayment::eWay;

# a test transaction
my ($tx, $txnum, $res);
ok($tx = new Business::OnlinePayment("eWay"));
ok(
    $tx->content(
        type           => 'CC',
        login          => '87654321', #ewayCustomerID
        action         => 'Normal Authorization',
        description    => 'Business::OnlinePayment test',
        amount         => '49.95',
        invoice_number => '100100',
        name           => 'Tofu Beast',
        card_number    => '4646464646464646',
        expiration     => '11/08',
    )
);
ok($tx->test_transaction(1));
ok($tx->submit());
ok($tx->is_success());
ok($tx->error_message(), '00, TRANSACTION APPROVED');
ok($tx->authorization(), '123456');
ok($res = $tx->server_response());
ok($res->{ewayReturnAmount}, 4995);
ok($txnum = $res->{ewayTrxnNumber});

#resubmit test
ok($tx->submit());
ok(($tx->server_response->{ewayTrxnNumber} - $txnum) > 0);

# a test transaction with cvn
ok(
    $tx->content(
        type           => 'CC',
        login          => '87654321', #ewayCustomerID
        action         => 'Normal Authorization',
        description    => 'Business::OnlinePayment test',
        amount         => '49.95',
        invoice_number => '100100',
        name           => 'Tofu Beast',
        card_number    => '4646464646464646',
        expiration     => '11/08',
	option1        => 'first option',
	cvv2           => '123',
    )
);
ok($tx->submit());
ok($tx->is_success());
ok($tx->error_message(), '00,TRANSACTION APPROVED');
ok($tx->authorization(), '123456');
ok($tx->server_response->{ewayTrxnOption1}, 'first option');

# a failing transaction
ok(
    $tx->content(
        type           => 'CC',
        login          => '87654321', #ewayCustomerID
        action         => 'Normal Authorization',
        description    => 'Business::OnlinePayment test',
        amount         => '49.95',
        invoice_number => '100100',
        name           => 'Tofu Beast',
        first_name     => 'Tofu',
        last_name      => 'Beast',
        email          => 'tofu@example.com',
        address        => '1234 Bean Curd Lane, Sydney',
        zip            => '2034',
        card_number    => '4646464646464646',
        expiration     => '11/08',
	cvv2           => '123',
    )
);
ok($tx->test_transaction(0),0);
ok($tx->submit());
ok($tx->is_success(),0);
ok($tx->error_message(), 'Error: Invalid eWAYCustomerID. You have not been billed for this transaction.');
ok($tx->authorization(), '123456');
